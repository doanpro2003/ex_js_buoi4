// câu1
document.getElementById('diem').onclick = function () {
    var diemchuan = document.getElementById('txt-num0').value * 1;
    var diem1 = document.getElementById('txt-num1').value * 1;
    var diem2 = document.getElementById('txt-num2').value * 1;
    var diem3 = document.getElementById('txt-num3').value * 1;
    var kv = document.getElementById('khuvuc');
    var dt = document.getElementById('people');
    if (diem1 == 0 || diem2 == 0 || diem3 == 0) {
        document.getElementById('kqDiem').innerHTML = "bạn đã rớt";
        return;
    }
    var khuvuc = kv.options[kv.selectedIndex].value
    var doituong = dt.options[dt.selectedIndex].value
    if (khuvuc == "select") {
        khuvuc = 0
    }
    if (dt.options[dt.selectedIndex].value == "select") {
        doituong = 0
    }

    var total = diem1 + diem2 + diem3 + khuvuc * 1 + doituong * 1
    if (total >= 30) {
        document.getElementById('kqDiem').innerHTML = "bạn đã đậu" + " tổng điểm: " + total;
    } else if (total < 30) {
        if (total >= diemchuan) {
            document.getElementById('kqDiem').innerHTML = "bạn đã đậu " + "tổng điểm: " + total;
        } else if (total < diemchuan) {
            document.getElementById('kqDiem').innerHTML = "bạn đã rớt." + " tông điểm: " + total;
        }
    }
}
// câu2
document.getElementById('tien-dien').onclick = function () {
    var name = document.getElementById('txt-name').value;
    var electric = document.getElementById('txt-electric').value * 1;
    var money;
    if (electric <= 50) {
        money = electric * 500;
    }
    else if (50 < electric && electric <= 100) {
        money = 50 * 500 + (electric - 50) * 650;
    }
    else if (100 < electric && electric <= 200) {
        money = 50 * 500 + 50 * 650 + (electric - 100) * 850;
    }
    else if (200 < electric && electric <= 350) {
        money = 50 * 500 + 50 * 650 + 100 * 850 + (electric - 200) * 1100;
    }
    else
        money = 50 * 500 + 50 * 650 + 100 * 850 + 150 * 1100 + (electric - 350) * 1300;
    money = new Intl.NumberFormat("vn-VN").format(money);
    document.getElementById("kqTienDien").innerHTML = "Họ tên: " + name + "; " + "Tiền điện: " + money;
}
// câu3
document.getElementById('tien-thue').onclick = function () {
    var name1 = document.getElementById('txt-name-1').value;
    var sumincome = document.getElementById('txt-income').value * 1;
    var numpeople = document.getElementById('txt-num-people').value * 1;
    var tax;
    if (sumincome <= 60000000) {

        tax = (sumincome - 4000000 - numpeople * 1600000) * 0.05;

    } else if (sumincome <= 120000000) {

        tax = (sumincome - 4000000 - numpeople * 1600000) * 0.1;

    } else if (sumincome <= 210000000) {

        tax = (sumincome - 4000000 - numpeople * 1600000) * 0.15;

    } else if (sumincome <= 384000000) {

        tax = (sumincome - 4000000 - numpeople * 1600000) * 0.2;

    } else if (sumincome <= 624000000) {

        tax = (sumincome - 4000000 - numpeople * 1600000) * 0.25;

    } else if (sumincome <= 960000000) {

        tax = (sumincome - 4000000 - numpeople * 1600000) * 0.3;

    } else {

        tax = (sumincome - 4000000 - numpeople * 1600000) * 0.35;

    }
    tax = new Intl.NumberFormat("vn-VN").format(tax);
    document.getElementById("kqTienThue").innerHTML = "Họ tên: " + name1 + "; " + "Tiền thuế thu nhập cá nhân: " + tax + " VND";
}
// câu4
function onChange() {
    var loaiKh = document.getElementById("loaikhachhang");
    var Kh = loaiKh.options[loaiKh.selectedIndex].value;
    if (Kh == "doanhnghiep") {
        document.getElementById("txt-connect").style.display = 'block';
    } else {
        document.getElementById("txt-connect").style.display = 'none';
    }
}
function tienNhaDan(channel) {
    var tienDichVu;
    tienDichVu = 4.5 + 20.5 + 7.5 * channel;
    return tienDichVu;
}
function tienDoanhNghiep(channel, numConnect) {
    var tienDichVu;
    if (numConnect <= 10) {
        tienDichVu = 15 + 75 + channel * 50;
    } else {
        tienDichVu = 15 + 75 + channel * 50 + (numConnect - 10) * 5;
    }
    return tienDichVu;
}
function tinhTienCap() {
    var sumMoney;
    var code = document.getElementById("txt-code").value;
    var channel = document.getElementById("txt-channel").value * 1;
    var numConnect = document.getElementById("txt-connect").value * 1;
    var loaiKhachHang = document.getElementById("loaikhachhang");
    var loaiKh = loaiKhachHang.options[loaiKhachHang.selectedIndex].value;
    if (loaiKh == "nhadan") sumMoney = tienNhaDan(channel);
    else if (loaiKh == "doanhnghiep")
        sumMoney = tienDoanhNghiep(channel, numConnect);
    sumMoney = new Intl.NumberFormat("en", {
        style: "currency",
        currency: "USD",
    }).format(sumMoney);
    document.getElementById("kqTienCap").innerHTML = "Mã khách hàng: " + code + ";" + " Tiền cáp: " + sumMoney;
}











